# GNU/Linux Audio Performance Kit

> Scripting, configs and other setup automation for using a DAW for live performance on GNU/Linux

This project is designed to support my gear, though much of the configuration should be the same regardless of which
controllers or other hardware is being used.

**Ideally, this work should completely automate setting up one or more PCs for audio performance from a fresh OS install.**

---

## Automation

[Ansible][href.ansible] playbooks are used to bring GNU/Linux machine targets to a state where audio and production applications
"just work".

## Kit

Here is the core software used in this setup:

### OS

> [CachyOS][href.cachyos] (64-bit) w/ [Hyprland][href.hyprland] (on [Wayland][href.wayland])

CachyOS has been great out of the box for running all of the audio applications I normally use. It is [Arch][href.arch-linux]-based, so packages are installed with [pacman][href.pacman].
**[Pipewire (libpipewire 1.x.x)][href.pipewire]** is enabled by default and has been perfect for low-latency I/O
with my audio interface.

I've used a similar setup with other desktop environments, like [i3][href.i3] on [X11][href.x11], so my specific installation isn't a 
hard requirement. Before moving to [CachyOS][href.cachyos] I was using [Kubuntu][href.kubuntu] 22.04 and getting similar
results. **Use what works best for your kit.**

### DAW

> [Bitwig Studio 5.x][href.bitwig]

Bitwig works great on GNU/Linux and doesn't require a compatibility tool like [Wine][href.wine] or [Bottles][href.bottles].

Bitwig also offers [support for user-authored extension scripts][href.bitwig.ext-scripts].
Jürgen Moßgraber has released a great set of these extensions in a collection called **[DrivenByMoss][href.driven-by-moss]**
which offers rich support for various MIDI controllers in Bitwig. The [Akai APC40 mkII][href.akai.apc40mkii], as an example,
gains 4 new modes beyond its normal clip launching grid and transport controls: MIDI sequencer, drum machine, raindrop
sequencer, and a playable keyboard. DrivenByMoss also includes scripts for things like gamepads, letting you use
your gaming controllers as configurable MIDI surfaces.

### Extras

#### [RaySession][href.raysession]

Virtual audio I/O connections between programs and interfaces are managed with RaySession. Applications with virtual
audio connections (DAWs, synths, virtual amps, RaySession daemons on other PCs on the local network, etc.) can be
configured together in an onscreen patchbay session, and saved for reproducible audio setups.

#### [Guitarix][href.guitarix]

A virtual electric guitar/bass guitar amplifier system. Comes with tons of plugins to build banks of custom tones right on
a PC. Can be easily integrated into RaySession to route guitar/bass signals to any DAW.

#### [Mixxx][href.mixxx]

A FOSS DJing application, also useful for organizing samples.

#### [SunVox][href.sunvox]

This tool by Alexander Zolotov is a modular synthesizer + tracker combined into an app that runs on virtually any system,
including Android and iOS. SunVox is great for developing sounds, samples, and full tracks, even when not at a normal
workstation. It can also be used as an instrument in another DAW.


<!-- Links -->
[href.cachyos]: https://cachyos.org/
[href.bitwig]: https://www.bitwig.com/
[href.hyprland]: https://hyprland.org/
[href.wayland]: https://wayland.freedesktop.org/
[href.arch-linux]: https://archlinux.org/
[href.pacman]: https://wiki.archlinux.org/title/pacman
[href.pipewire]: https://pipewire.org/
[href.i3]: https://i3wm.org/
[href.x11]: https://en.wikipedia.org/wiki/X_Window_System
[href.kubuntu]: https://kubuntu.org/
[href.wine]: https://www.winehq.org/
[href.bottles]: https://usebottles.com/
[href.driven-by-moss]: https://github.com/git-moss/DrivenByMoss
[href.bitwig.ext-scripts]: https://www.bitwig.com/support/technical_support/community-controller-extensions-and-scripts-29/
[href.akai.apc40mkii]: https://www.akaipro.com/apc40-mkii
[href.sunvox]: https://warmplace.ru/soft/sunvox/
[href.raysession]: https://github.com/Houston4444/RaySession
[href.guitarix]: https://guitarix.org/
[href.mixxx]: https://mixxx.org/
[href.ansible]: https://github.com/ansible/ansible